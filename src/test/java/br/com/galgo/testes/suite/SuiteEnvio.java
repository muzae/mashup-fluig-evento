package br.com.galgo.testes.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import br.com.galgo.enviar.arquivo.EnviarExtratoArquivo;
import br.com.galgo.enviar.arquivo.EnviarInformacaoAnbimaArquivo;
import br.com.galgo.enviar.arquivo.EnviarPlCotaArquivo;
import br.com.galgo.enviar.arquivo.EnviarPosicaoAtivosArquivo2;
import br.com.galgo.enviar.portal.EnviarExtratoPortal;
import br.com.galgo.enviar.portal.EnviarInformacaoAnbimaPortal;
import br.com.galgo.enviar.portal.EnviarPlCotaPortal;
import br.com.galgo.testes.recursos_comuns.suite.StopOnFirstFailureSuite;

@RunWith(StopOnFirstFailureSuite.class)
@Suite.SuiteClasses({ EnviarPlCotaPortal.class,//
		EnviarInformacaoAnbimaPortal.class,//
		EnviarExtratoPortal.class, //
		EnviarPlCotaArquivo.class,//
		EnviarInformacaoAnbimaArquivo.class,//
		EnviarExtratoArquivo.class,//
		EnviarPosicaoAtivosArquivo2.class //
})
public class SuiteEnvio {

}
